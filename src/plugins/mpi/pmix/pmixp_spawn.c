/*****************************************************************************\
 **  pmixp_spawn.cpp - PMIx spawn functionality
 *****************************************************************************
 *  Copyright (C) 2023 René Pascal Becker. All rights reserved.
 *  Written by René Pascal Becker <rene.becker2@gmx.de>,
 *
 *  This file is part of Slurm, a resource management program.
 *  For details, see <https://slurm.schedmd.com/>.
 *  Please also read the included file: DISCLAIMER.
 *
 *  Slurm is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *
 *  In addition, as a special exception, the copyright holders give permission
 *  to link the code of portions of this program with the OpenSSL library under
 *  certain conditions as described in each individual source file, and
 *  distribute linked combinations including the two. You must obey the GNU
 *  General Public License in all respects for all of the code used other than
 *  OpenSSL. If you modify file(s) with this exception, you may extend this
 *  exception to your version of the file(s), but you are not obligated to do
 *  so. If you do not wish to do so, delete this exception statement from your
 *  version.  If you delete this exception statement from all source files in
 *  the program, then also delete it here.
 *
 *  Slurm is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with Slurm; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA.
 \*****************************************************************************/

#include "pmixp_spawn.h"
#include "pmixp_client.h"
#include "pmixp_coll.h"
#include "pmixp_common.h"
#include "pmixp_debug.h"
#include "pmixp_dmdx.h"
#include "pmixp_info.h"
#include "pmixp_io.h"
#include "pmixp_nspaces.h"
#include "pmixp_server.h"
#include "pmixp_state.h"
#include "pmixp_utils.h"

#include "src/common/slurm_xlator.h"
#include "src/common/xmalloc.h"
#include "src/common/xstring.h"

#include <endian.h>
#include <netdb.h>
#include <pmix_common.h>
#include <pmix_deprecated.h>
#include <pmix_server.h>
#include <pthread.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>

#define THESIS_LOG                                                             \
  FILE *ptr = fopen("/home/pmix", "a");                                        \
  fprintf(ptr, "%s:%d called\n", __FILE__, __LINE__);                          \
  fclose(ptr);

#define DPM_AGENT_SPAWN_MSG 0x90
#define DPM_AGENT_KEYSTORE_MSG 0x100
#define DPM_AGENT_CONNECT_MSG 0x110
#define DPM_AGENT_DMODEX_REQ_MSG 0x112
#define DPM_AGENT_DMODEX_RESP_MSG 0x113

enum KeyStoreRequests { STORE_PUBLISH, STORE_LOOKUP, STORE_UNPUBLISH };

struct PmixInfoData {
  char *key;
  pmix_value_t value;
  int timeout;
  pmix_persistence_t persistance;
};

// DPM ----

int _connect_to_dpm_agent() {
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  const char *host_ip = "localhost";
  const char *agent_port = getenv("DPM_AGENT_PORT");
  if (NULL == agent_port) {
    THESIS_LOG
    return 0;
  }

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;
  hints.ai_protocol = IPPROTO_TCP;

  int error_code = getaddrinfo(host_ip, agent_port, &hints, &result);
  if (0 != error_code) {
    THESIS_LOG
    return 0;
  }

  int socket_fd = 0;
  for (rp = result; rp != NULL; rp = rp->ai_next) {
    socket_fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (socket_fd == -1) {
      continue;
    }

    if (0 == connect(socket_fd, rp->ai_addr, rp->ai_addrlen)) {
      break; // successfully connected
    } else {
      close(socket_fd); // close socket when we cannot connect
    }
  }
  freeaddrinfo(result); // free result
  // exit if the result-linked-list has been traversed until the end without a
  // successful connection
  if (NULL == rp) {
    THESIS_LOG
    return 0;
  }
  return socket_fd;
}

char *_create_message(int message_id, char *data) {
  const int padding_size = 64;
  const int msg_size = strlen(data) + padding_size;
  char *msg = (char *)malloc(msg_size);
  memset(msg, 0, msg_size);

  snprintf(msg, msg_size, "{\"msg_type\": %d, \"msg_data\": \"%s\"}",
           message_id, data);

  return msg;
}

void _destroy_message(char *msg) { free(msg); }

int _send_message(int socket_fd, int message_id, char *data) {
  int result = 0;
  char *msg = _create_message(message_id, data);

  // Send the message itself
  uint32_t msg_length = strlen(msg) + 1;
  uint32_t le_msg_length = htole32(msg_length);
  if (send(socket_fd, &le_msg_length, sizeof(le_msg_length), 0) < 0)
    goto sendMessageFail;
  if (send(socket_fd, msg, msg_length, 0) < 0)
    goto sendMessageFail;

  // Cleanup
  _destroy_message(msg);
  return 1;
sendMessageFail:
  _destroy_message(msg);
  return 0;
}

char *_receive_message(int socket_fd) {
  uint32_t msg_length = 0;
  int ec = recv(socket_fd, &msg_length, sizeof(msg_length), 0);
  if (ec < 0) {
    FILE *ptr = fopen("/home/pmix", "a");
    fprintf(ptr, "Failed Read EC: %d\n", ec);
    fclose(ptr);
    return NULL;
  }

  msg_length = le32toh(msg_length);
  char *data = (char *)malloc(msg_length);
  ec = recv(socket_fd, data, msg_length, 0);
  if (ec < 0) {
    FILE *ptr = fopen("/home/pmix", "a");
    fprintf(ptr, "Failed Read EC: %d\n", ec);
    fclose(ptr);
    free(data);
    return NULL;
  }
  return data;
}

char *_cpy_message_data(char *json) {
  // Look for data in the JSON message.
  const char msg_data_key[] = "msg_data\":\"";
  char *start = strstr(json, msg_data_key) + strlen(msg_data_key);

  // Get length of data
  int len = 0;
  while (start[len] != '\"')
    len++;
  len++;

  char *result = (char *)malloc(len);
  strlcpy(result, start, len);
  result[len - 1] = '\0';
  return result;
}

struct PmixInfoData _parse_info_data(const pmix_info_t info[], size_t ninfo) {
  // Setup default info data struct
  struct PmixInfoData result;
  result.persistance = PMIX_PERSIST_APP;
  result.key = NULL;
  result.timeout = 120;

  // Parse possible infos
  for (size_t i = 0; i < ninfo; i++) {
    const pmix_info_t *current = &info[i];

    // Debug
    FILE *ptr = fopen("/home/pmix", "a");
    fprintf(ptr, "key: %s\nvalue.type:%d\n", current->key,
            (int)current->value.type);
    fclose(ptr);

    if (strcmp(current->key, PMIX_PERSISTENCE) == 0)
      result.persistance = current->value.data.persist;
    else if (strcmp(current->key, PMIX_TIMEOUT) == 0)
      result.timeout = current->value.data.int32;
    else if (strncmp(current->key, "pmix.", 5) != 0) {
      // This is the custom data
      result.key = (char *)current->key;
      result.value = current->value;
    }
  }
  return result;
}

// Binary to Hex
char *_create_byte_array(char *data, size_t in_sz, size_t *out_sz) {
  *out_sz = in_sz * 2;
  char *arr = (char *)malloc(*out_sz + 1);
  memset((void *)arr, 0, *out_sz + 1);
  uint32_t cursor = 0;
  for (unsigned int i = 0; i < in_sz; i++, cursor += 2)
    sprintf(&arr[cursor], "%02X", (unsigned int)data[i]);
  return arr;
}

// Hex to binary
char *_read_byte_array(const char *byte_array, size_t *out_sz) {
  *out_sz = strlen(byte_array) / 2;
  char *arr = (char *)malloc(*out_sz);
  memset((void *)arr, 0, *out_sz);
  uint32_t cursor = 0;
  for (unsigned int i = 0; i < strlen(byte_array); i += 2, cursor++) {
    int r;
    sscanf(&byte_array[i], "%02X", &r);
    arr[cursor] = r;
  }
  return arr;
}

void _get_sz_and_data(const pmix_value_t *value, size_t *out_sz,
                      char **out_data) {
  switch (value->type) {
  case PMIX_STRING:
    *out_sz = strlen(value->data.string) + 1;
    *out_data = value->data.string;
    break;
  case PMIX_BYTE_OBJECT:
    *out_sz = value->data.bo.size;
    *out_data = value->data.bo.bytes;
    break;
  default: {
    THESIS_LOG
    *out_sz = 0;
    *out_data = NULL;
  } break;
  }
  return;
}

void _set_value(pmix_value_t *value, char *data, size_t data_size,
                size_t type) {
  value->type = type;
  switch (type) {
  case PMIX_STRING:
    value->data.string = data;
    break;
  case PMIX_BYTE_OBJECT:
    value->data.bo.bytes = data;
    value->data.bo.size = data_size;
    break;
  default: {
    THESIS_LOG
    value->type = 0;
  } break;
  }
  return;
}

char *encode_pmix_value(const pmix_value_t *value, size_t *out_sz) {
  char *data;
  _get_sz_and_data(value, out_sz, &data);
  if (*out_sz == 0)
    return NULL;
  return _create_byte_array(data, *out_sz, out_sz);
}

pmix_value_t decode_pmix_value(const char *encoded, size_t *out_sz,
                               size_t type) {
  char *binary_data = _read_byte_array(encoded, out_sz);
  pmix_value_t value;
  _set_value(&value, binary_data, *out_sz, type);
  return value;
}

// PUBLISH ----

int publish_to_dpm_agent(const pmix_proc_t *proc, const pmix_info_t info[],
                         size_t ninfo) {
  const int max_publish_size = 2048;

  int socket_fd = _connect_to_dpm_agent();
  if (!socket_fd)
    return 0;

  struct PmixInfoData info_data = _parse_info_data(info, ninfo);
  if (!info_data.key) {
    close(socket_fd);
    THESIS_LOG
    return 0;
  }

  // Encode data
  size_t encoded_data_size;
  char *encoded_data = encode_pmix_value(&info_data.value, &encoded_data_size);
  if (encoded_data_size == 0 || encoded_data == NULL) {
    close(socket_fd);
    THESIS_LOG
    return 0;
  }

  _debug_proc(proc, "Publishing %s with value %s", info_data.key, encoded_data);

  // Create data to send
  char data[max_publish_size];
  memset((void *)data, 0, max_publish_size);
  snprintf(data, max_publish_size, "%d,%s,%zu,%s,%hu,%s", (int)STORE_PUBLISH,
           (const char *)proc->nspace, (size_t)proc->rank, info_data.key,
           info_data.value.type, encoded_data);
  free(encoded_data);

  if (!_send_message(socket_fd, DPM_AGENT_KEYSTORE_MSG, data)) {
    close(socket_fd);
    return 0;
  }

  // Receive result
  char *status_msg = _receive_message(socket_fd);
  if (!status_msg) {
    close(socket_fd);
    return 0;
  }
  char *status_data = _cpy_message_data(status_msg);
  _destroy_message(status_msg);

  // Check result
  if (strcmp(status_data, "0") != 0 || strlen(status_data) != 1) {
    _destroy_message(status_data);
    close(socket_fd);
    return 0;
  }

  _destroy_message(status_data);
  close(socket_fd);
  return 1;
}

// LOOKUP ----

struct ReceiverInfo {
  pmix_lookup_cbfunc_t cbfunc;
  void *cbdata;
  int socket_fd;
};

void _parse_next_data_val(pmix_pdata_t *data_obj) {
  char *key = strtok(NULL, ",");
  char *value_type = strtok(NULL, ",");
  char *value = strtok(NULL, ",");
  char *nspace = strtok(NULL, ",");
  char *rank = strtok(NULL, ",");

  size_t out_sz, type;
  sscanf(value_type, "%zu", &type);
  data_obj->value = decode_pmix_value(value, &out_sz, type);

  strcpy((char *)data_obj->key, key);
  strcpy((char *)data_obj->proc.nspace, nspace);
  sscanf(rank, "%u", &data_obj->proc.rank);
}

void _parse_data_arr(char *msg, pmix_pdata_t **data_arr,
                     unsigned long *ndata_arr) {
  char *count = strtok(msg, ",");
  if (!count)
    return;

  sscanf(count, "%lu", ndata_arr);
  PMIX_PDATA_CREATE(*data_arr, *ndata_arr);

  for (unsigned long i = 0; i < *ndata_arr; i++) {
    _parse_next_data_val(&((*data_arr)[i]));
  }
}

void *_lookup_receiver(void *data) {
  struct ReceiverInfo *info = (struct ReceiverInfo *)data;

  // Receive result
  char *data_msg = _receive_message(info->socket_fd);
  if (!data_msg)
    goto receiverCleanup;

  // Fetch message string
  char *result = _cpy_message_data(data_msg);
  _destroy_message(data_msg);

  _debug("Recv from Lookup: %s", result);

  // Parse Data
  pmix_pdata_t *data_arr;
  unsigned long ndata_arr;
  _parse_data_arr(result, &data_arr, &ndata_arr);
  info->cbfunc(PMIX_SUCCESS, data_arr, ndata_arr, info->cbdata);
  _destroy_message(result);

  // Cleanup
receiverCleanup:
  close(info->socket_fd);
  free(info);
  return NULL;
}

int lookup_from_dpm_agent(const pmix_proc_t *proc, const pmix_info_t info[],
                          size_t ninfo, char **keys,
                          pmix_lookup_cbfunc_t cbfunc, void *cbdata) {
  const int max_lookup_size = 2048;

  int socket_fd = _connect_to_dpm_agent();
  if (!socket_fd)
    return 0;

  struct PmixInfoData info_data = _parse_info_data(info, ninfo);
  int keyIndex = 0;
  char *key = NULL;
  while (keys[keyIndex]) {
    key = keys[keyIndex];
    keyIndex++;
  }

  if (!key) {
    close(socket_fd);
    THESIS_LOG
    return 0;
  }

  _debug_proc(proc, "Lookup for key: %s", key);

  // Create data to send
  char data[max_lookup_size];
  memset((void *)data, 0, max_lookup_size);
  snprintf(data, max_lookup_size, "%d,%s,%zu,%s", (int)STORE_LOOKUP,
           (const char *)proc->nspace, (size_t)proc->rank, key);

  if (!_send_message(socket_fd, DPM_AGENT_KEYSTORE_MSG, data)) {
    close(socket_fd);
    return 0;
  }

  // Create thread to receive asynchronously
  pthread_t th1;
  struct ReceiverInfo *recv_info =
      (struct ReceiverInfo *)malloc(sizeof(struct ReceiverInfo));
  recv_info->cbdata = cbdata;
  recv_info->cbfunc = cbfunc;
  recv_info->socket_fd = socket_fd;
  pthread_create(&th1, NULL, _lookup_receiver, recv_info);
  pthread_detach(th1);
  return 1;
}

// DMODEX DAEMON

struct ModexResultInfo {
  int socket_fd;
  int rank;
  pmix_nspace_t nspace;
  char *node;
  uint64_t transaction;
};

void _handle_dmodex_result(pmix_status_t status, char *data, size_t sz,
                           void *cbdata) {
  struct ModexResultInfo *info = (struct ModexResultInfo *)cbdata;
  char *msg_data = NULL;

  // Initialize the header data
  const size_t max_dmodex_resp_len = 512;
  char header[max_dmodex_resp_len];
  memset((void *)header, 0, max_dmodex_resp_len);
  snprintf(header, max_dmodex_resp_len, "%s,%u,%s,%lu,", info->nspace,
           info->rank, info->node, info->transaction);

  // Sanitize data
  size_t cursor;
  char *sanitized_data = _create_byte_array(data, sz, &cursor);

  // Setup header in final message
  uint32_t msg_data_len = strlen(header) + cursor + 1;
  msg_data = (char *)malloc(msg_data_len);
  memset((void *)msg_data, 0, msg_data_len);
  strcpy(msg_data, header);

  // Add dmodex data
  memcpy(&msg_data[strlen(header)], sanitized_data, cursor);
  free(sanitized_data);

  // ... and send the message!
  _send_message(info->socket_fd, DPM_AGENT_DMODEX_RESP_MSG, msg_data);
  free(msg_data);
  free(info->node);
  free(info);
}

bool stop_dmodex_daemon = false;
void *dmodex_daemon(void *dpm_socket) {
  int socket_fd = (int)(size_t)dpm_socket;
  stop_dmodex_daemon = false;
  if (!socket_fd)
    return NULL;

  struct pollfd fd;
  fd.fd = socket_fd;
  fd.events = POLLIN;
  FILE *ptr = fopen("/home/dmodex", "a");
  fprintf(ptr, "START\n");
  fclose(ptr);

  while (!stop_dmodex_daemon) {
    int rv = 0;
    if ((rv = poll(&fd, 1, 100) > 0)) {
      ptr = fopen("/home/dmodex", "a");
      fprintf(ptr, "RESULT: %d\n", fd.revents);
      fclose(ptr);
      if (fd.revents & POLLIN) {

        ptr = fopen("/home/dmodex", "a");
        fprintf(ptr, "POLLIN\n");
        fclose(ptr);
        // New message to be read
        char *data_msg = _receive_message(socket_fd);
        if (!data_msg) {
          continue;
        }

        // Fetch message string
        char *msg = _cpy_message_data(data_msg);
        _destroy_message(data_msg);
        ptr = fopen("/home/dmodex", "a");
        fprintf(ptr, "%s\n", msg);
        fclose(ptr);

        // Parse Data
        char *nspace = strtok(msg, ",");
        if (nspace == NULL) {
          _destroy_message(msg);
          continue;
        }
        char *rank = strtok(NULL, ",");
        if (rank == NULL) {
          _destroy_message(msg);
          continue;
        }
        char *node = strtok(NULL, ",");
        if (node == NULL) {
          _destroy_message(msg);
          continue;
        }
        char *transaction = strtok(NULL, ",");
        if (transaction == NULL) {
          _destroy_message(msg);
          continue;
        }

        ptr = fopen("/home/dmodex", "a");
        fprintf(ptr, "Received Request %s,%s\n", nspace, rank);
        fclose(ptr);

        // Fill proc and submit dmodex request to the server
        pmix_proc_t proc;
        sscanf(rank, "%u", &proc.rank);
        strlcpy(proc.nspace, nspace, PMIX_MAX_NSLEN);

        // Call the server
        struct ModexResultInfo *info =
            (struct ModexResultInfo *)malloc(sizeof(struct ModexResultInfo));
        strlcpy(info->nspace, nspace, PMIX_MAX_NSLEN);
        info->rank = proc.rank;
        info->socket_fd = socket_fd;
        info->node = strdup(node);
        sscanf(transaction, "%lu", &info->transaction);
        int error = PMIx_server_dmodex_request(&proc, _handle_dmodex_result,
                                               (void *)info);
        if (error != PMIX_SUCCESS)
          free(info);
        ptr = fopen("/home/dmodex", "a");
        fprintf(ptr, "Request error code: %d\n", error);
        fclose(ptr);

        _destroy_message(msg);

      } else if (fd.revents & POLLERR || fd.revents & POLLHUP ||
                 fd.revents & POLLNVAL) {

        FILE *ptr = fopen("/home/dmodex", "a");
        fprintf(ptr, "ERROR: %d, %d, %d\n", fd.revents & POLLERR,
                fd.revents & POLLHUP, fd.revents & POLLNVAL);
        fclose(ptr);
        stop_dmodex_daemon = true;
      }
    }
  }

  ptr = fopen("/home/dmodex", "a");
  fprintf(ptr, "STOP\n");
  fclose(ptr);
  close(socket_fd);
  return NULL;
}

// CONNECT ----

struct CollectConnectInfo {
  pmix_op_cbfunc_t cbfunc;
  void *cbdata;
  int socket_fd;
};

void *_collect_connect_receiver(void *data) {
  struct CollectConnectInfo *info = (struct CollectConnectInfo *)data;

  // Receive result
  char *data_msg = _receive_message(info->socket_fd);
  if (!data_msg)
    goto collectConnectCleanup;

  // Fetch message string
  char *result = _cpy_message_data(data_msg);
  _destroy_message(data_msg);

  // Check result
  if (strcmp(result, "0") != 0 || strlen(result) != 1) {
    info->cbfunc(PMIX_ERROR, info->cbdata);
    _destroy_message(result);
    goto collectConnectCleanup;
  } else
    info->cbfunc(PMIX_SUCCESS, info->cbdata);
  _destroy_message(result);

  // Startup the dmodex thread
  pthread_t th;
  pthread_create(&th, NULL, dmodex_daemon, (void *)((size_t)info->socket_fd));
  pthread_detach(th);
  free(info);
  return NULL;

  // Cleanup
collectConnectCleanup:
  close(info->socket_fd);
  free(info);
  return NULL;
}

int collect_connected(const pmix_proc_t procs[], size_t proc_count,
                      const pmix_info_t infos[], size_t info_count,
                      pmix_op_cbfunc_t cbfunc, void *cbdata) {
  // Check if we actually have any data to send
  if (!proc_count)
    return 0;

  char *msg_data = NULL;

  // Initialize the first data
  const size_t max_proc_info_len = 128;
  char data[max_proc_info_len];
  memset((void *)data, 0, max_proc_info_len);
  snprintf(data, sizeof data, "%s,%u", (const char *)procs[0].nspace,
           procs[0].rank);
  msg_data = (char *)malloc(strlen(data) + 1);
  strcpy(msg_data, data);

  // Append other procs
  for (size_t i = 1; i < proc_count; i++) {
    memset((void *)data, 0, max_proc_info_len);
    snprintf(data, sizeof data, ",%s,%u", (const char *)procs[i].nspace,
             procs[i].rank);
    char *old_data = msg_data;
    msg_data = (char *)malloc(strlen(data) + strlen(old_data) + 1);
    strcpy(msg_data, old_data);
    strcat(msg_data, data);
    free(old_data);
  }

  // Connect to agent...
  int socket_fd = _connect_to_dpm_agent();
  if (!socket_fd) {
    free(msg_data);
    return 0;
  }

  _debug("Procs trying to collect connect.");

  // ... and send the message!
  if (!_send_message(socket_fd, DPM_AGENT_CONNECT_MSG, msg_data)) {
    free(msg_data);
    close(socket_fd);
    return 0;
  }
  free(msg_data);

  // Create thread to receive asynchronously
  pthread_t th1;
  struct CollectConnectInfo *connect_info =
      (struct CollectConnectInfo *)malloc(sizeof(struct CollectConnectInfo));
  connect_info->cbdata = cbdata;
  connect_info->cbfunc = cbfunc;
  connect_info->socket_fd = socket_fd;
  pthread_create(&th1, NULL, _collect_connect_receiver, connect_info);
  pthread_detach(th1);
  return 1;
}

// DMODEX ----

struct DmodexInfo {
  pmix_modex_cbfunc_t cbfunc;
  void *cbdata;
  int socket_fd;
};

void *_modex_data_receiver(void *data) {
  struct DmodexInfo *info = (struct DmodexInfo *)data;

  // Receive result
  char *data_msg = _receive_message(info->socket_fd);
  if (!data_msg)
    goto modexCleanup;

  // Fetch message string
  char *result = _cpy_message_data(data_msg);
  FILE *ptr = fopen("/home/dmodex_recv", "a");
  fprintf(ptr, "RECEIVED %s\n", result);
  fclose(ptr);
  _destroy_message(data_msg);

  // Get modex data
  strtok(result, ",");                     // namespace
  strtok(NULL, ",");                       // rank
  strtok(NULL, ",");                       // node
  strtok(NULL, ",");                       // transaction
  char *msg_modexData = strtok(NULL, ","); // data
  size_t modexDataSz;
  char *modexData = _read_byte_array(msg_modexData, &modexDataSz);
  _destroy_message(result);

  // Pass to pmix
  info->cbfunc(PMIX_SUCCESS, modexData, modexDataSz, info->cbdata, free,
               modexData);

  // Cleanup
modexCleanup:
  close(info->socket_fd);
  free(info);
  return NULL;
}

int retrieve_dmodex_data(const pmix_proc_t *proc, const pmix_info_t info[],
                         size_t ninfo, pmix_modex_cbfunc_t cbfunc,
                         void *cbdata) {
  char *msg_data = NULL;

  // Initialize the message data
  const size_t max_dmodex_req_len = 512;
  char data[max_dmodex_req_len];
  memset((void *)data, 0, max_dmodex_req_len);
  snprintf(data, max_dmodex_req_len, "%s,%u", (const char *)proc->nspace,
           proc->rank);
  msg_data = (char *)malloc(strlen(data) + 1);
  strcpy(msg_data, data);

  // Connect to agent...
  int socket_fd = _connect_to_dpm_agent();
  if (!socket_fd) {
    free(msg_data);
    return 0;
  }

  // ... and send the message!

  FILE *ptr = fopen("/home/dmodex", "a");
  fprintf(ptr, "Sending Request: %s\n", msg_data);
  fclose(ptr);
  if (!_send_message(socket_fd, DPM_AGENT_DMODEX_REQ_MSG, msg_data)) {
    free(msg_data);
    close(socket_fd);
    return 0;
  }
  free(msg_data);

  // Create thread to receive asynchronously
  pthread_t th1;
  struct DmodexInfo *modex_info =
      (struct DmodexInfo *)malloc(sizeof(struct CollectConnectInfo));
  modex_info->cbdata = cbdata;
  modex_info->cbfunc = cbfunc;
  modex_info->socket_fd = socket_fd;
  pthread_create(&th1, NULL, _modex_data_receiver, modex_info);
  pthread_detach(th1);
  return 1;
}

// SPAWN ----

char **_create_srun_argv(const pmix_app_t *app, int *_out_app_argc) {
  const int max_srun_args = 7;
  int app_argc = 0;
  while (app->argv[app_argc])
    app_argc++;
  (*_out_app_argc) = app_argc;

  char **argv = NULL;
  xrealloc(argv, (max_srun_args + app_argc) * sizeof(char *));
  return argv;
}

void _populate_srun_argv(const pmix_app_t *app, int app_argc, char **argv,
                         size_t subtask_num) {
  int index = 0;
  argv[index++] = "srun";
  argv[index++] = "--mpi=pmix";
  xstrfmtcat(argv[index++], "--output=/home/subtask%zu.out", subtask_num);
  xstrfmtcat(argv[index++], "--ntasks=%d", app->maxprocs);

  for (size_t i = 0; i < app->ninfo; i++) {
    if (xstrcmp(PMIX_WDIR, app->info[i].key) == 0) {
      xstrfmtcat(argv[index++], "--chdir=%s", app->info[i].value.data.string);
    } else {
      THESIS_LOG
      abort();
    }
  }

  for (int i = 0; i < app_argc; i++) {
    argv[index++] = app->argv[i];
  }
}

int _validate_launch(const pmix_proc_t *proc, const pmix_app_t apps[],
                     size_t napps, size_t **_out_ids, size_t *_out_nids,
                     char **_out_vrm_id) {
  // Make sure we do not launch "nothing"
  if (!napps)
    return 0;

  char *msg_data = NULL;

  // Initialize the start of the data
  const size_t max_spawn_info_len = 128;
  char data[max_spawn_info_len];
  memset((void *)data, 0, max_spawn_info_len);
  snprintf(data, max_spawn_info_len, "%s,%u,%u", (const char *)proc->nspace,
           proc->rank, pmixp_info_jobid());
  msg_data = (char *)malloc(strlen(data) + 1);
  strcpy(msg_data, data);

  // Append apps
  for (size_t i = 0; i < napps; i++) {
    memset((void *)data, 0, max_spawn_info_len);
    snprintf(data, max_spawn_info_len, ",%s,%u", (const char *)apps[i].cmd,
             apps[i].maxprocs);
    char *old_data = msg_data;
    msg_data = (char *)malloc(strlen(data) + strlen(old_data) + 1);
    strcpy(msg_data, old_data);
    strcat(msg_data, data);
    free(old_data);
  }

  // Connect to agent...
  int socket_fd = _connect_to_dpm_agent();
  if (!socket_fd) {
    free(msg_data);
    return 0;
  }

  // ... and send the message!
  if (!_send_message(socket_fd, DPM_AGENT_SPAWN_MSG, msg_data)) {
    free(msg_data);
    close(socket_fd);
    return 0;
  }
  free(msg_data);

  // Receive assign response
  char *msg = _receive_message(socket_fd);
  close(socket_fd);
  if (!msg)
    return 0;

  msg_data = _cpy_message_data(msg);
  _destroy_message(msg);

  // Ignore namespace, rank
  strtok(msg_data, ",");
  strtok(NULL, ",");

  // Check if we are allowed to proceed
  if (strcmp(strtok(NULL, ","), "0") == 0) {
    _destroy_message(msg_data);
    return 0;
  }

  // Fetch vrm job id
  char *vrm_id = strtok(NULL, ",");
  *_out_vrm_id = (char *)malloc(strlen(vrm_id) + 1);
  strcpy(*_out_vrm_id, vrm_id);

  // Ignore nonce
  strtok(NULL, ",");

  // Parse all assigned ids
  char *count = strtok(NULL, ",");
  uint32_t numTasks = 0;
  sscanf(count, "%u", &numTasks);
  size_t *ids = (size_t *)malloc(sizeof(size_t) * numTasks);
  for (uint32_t i = 0; i < numTasks; i++)
    sscanf(strtok(NULL, ","), "%zu", &ids[i]);
  *_out_ids = ids;
  *_out_nids = numTasks;

  _destroy_message(msg_data);
  return 1;
}

void _launch_app(const pmix_proc_t *parent, const pmix_app_t *app,
                 size_t dynamicId, char *vrm_job_id) {
  int app_argc;
  char **argv = _create_srun_argv(app, &app_argc);
  _populate_srun_argv(app, app_argc, argv, dynamicId);

  size_t index = 0;
  FILE *ptr = fopen("/home/pmix", "a");
  fprintf(ptr, "SPAWN ENVIRONMENT: \n");
  while (app->env[index]) {
    fprintf(ptr, "\tENV: %s\n", app->env[index]);
    index++;
  }
  index = 0;
  while (argv[index]) {
    fprintf(ptr, "\tARG: %s\n", argv[index]);
    index++;
  }

  fprintf(ptr, "Namespace: %s\n", pmixp_info_namespace());

  fclose(ptr);

  char **env = env_array_copy(app->env);
  env_array_append_fmt(&env, "HOME", "/root");
  env_array_append_fmt(&env, "DPM_AGENT_PORT", "25000");
  env_array_append_fmt(&env, "SLURM_VRM_JOBID", "%s", vrm_job_id);
  env_array_append_fmt(&env, "DPM_PMIX_DYNAMIC_ID", "%zu", dynamicId);
  execve(SLURM_PREFIX "/bin/srun", argv, env);
  abort();
}

int pmixp_spawn(const pmix_proc_t *proc, const pmix_app_t apps[], size_t napps,
                pmix_spawn_cbfunc_t cbfunc, void *cbdata) {
  // First ask for permission
  size_t *dynamicIds;
  char *vrm_job_id = NULL;
  size_t nDynamicIds;
  if (!_validate_launch(proc, apps, napps, &dynamicIds, &nDynamicIds,
                        &vrm_job_id))
    return 0;

  size_t currentApp = 0;

  THESIS_LOG
  ptr = fopen("/home/pmix", "a");

  fprintf(ptr, "NAPPS2: %d\n", napps);
  for (size_t i = 0; i < napps; i++) {
    pmix_app_t *app = &apps[i];
    fprintf(ptr, "App: %s\n", app->cmd);
    fprintf(ptr, "Max procs: %d\n", app->maxprocs);

    for (size_t j = 0; j < app->ninfo; j++) {
      fprintf(ptr, "\tAppInfo Key: %s\n", app->info[j].key);
    }
    size_t index = 0;
    while (app->argv[index]) {
      fprintf(ptr, "ARG: %s\n", app->argv[index]);
      index++;
    }

    __pid_t pid = fork();
    if (pid == 0) {
      _launch_app(proc, app, dynamicIds[currentApp], vrm_job_id);
    }
    currentApp++;
  }
  free(dynamicIds);
  free(vrm_job_id);
  fclose(ptr);
  return 1;
}